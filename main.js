var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');

var x = 0;
var y = 0;
var current=1;
var cPushArray = new Array();
var cStep = -1;
var startX,startY;
var line=true,linef=1;
var triangle=false,trf=0;
var rectangle=false,recf=0;
var circle=false,cirf=0;
var erass=0;
var textt=false;
var imagg=false;
var change=0;
var sf=0;

var flag=1;
var shadoww=0;
var gg=0;

var imageLoader = document.getElementById('imageLoader');
imageLoader.addEventListener('change', handleImage, false);

function gradient(){
  gg=1;
}
function nogradient(){
  gg=0;
}

function textstroke(){
  sf=1;
}
function textfill(){
  sf=0;
}

function handleImage(e){
  var reader = new FileReader();
  reader.onload = function(event){
    var img = new Image();
    img.onload = function(){
      document.getElementById('canvas').addEventListener('click', function(evt) {
        if(flag===0){
        var mousePos = getMousePos(canvas, evt);
        ctx.drawImage(img,mousePos.x,mousePos.y);
        }
        
      }, false);
    }
    img.src = event.target.result;
  }
  reader.readAsDataURL(e.target.files[0]);  
  
}


function stick(){
  line=false;
  triangle=false;
  rectangle=false;
  circle=false;
  textt=false;
  imagg=true;
  flag=0;
}


function text(){
  line=false;
  triangle=false;
  rectangle=false;
  circle=false;
  textt=true;
  imagg=false;
  flag=1;
  erass=0;
}

function drawLine(){
  line=true;
  triangle=false;
  rectangle=false;
  circle=false;
  trf=0;
  recf=0;
  cirf=0;
  linef=1;
  textt=false;
  imagg=false;
  flag=1;
}

function drawCircle(){
  line=false;
  triangle=false;
  rectangle=false;
  circle=true;
  trf=0;
  recf=0;
  cirf=1;
  linef=0;
  textt=false;
  imagg=false;
  flag=1;
}

function shadow(){
  shadoww=1;
}

function drawTriangle(){
  line=false;
  triangle=true;
  rectangle=false;
  circle=false;
  trf=1;
  recf=0;
  cirf=0;
  linef=0;
  textt=false;
  imagg=false;
  flag=1;
}

function drawRectangle(){
  line=false;
  triangle=false;
  rectangle=true;
  circle=false;
  trf=0;
  recf=1;
  cirf=0;
  linef=0;
  textt=false;
  imagg=false;
  flag=1;
}

function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  //getBoundingClientRect 取得物件完整座標資訊，包含寬高等
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };   
  //這個function將會傳回滑鼠在 _canvas上的座標
};



function mouseMove(evt) {
  var mousePos = getMousePos(canvas, evt);
  //透過getMousePos function 去取得滑鼠座標
  //mousePos 是一個物件，包含x和y的值

  if(erass===1 && flag===1)
  {
    ctx.shadowBlur=0;
    ctx.lineWidth=20;
    ctx.strokeStyle="#ffffff"
    ctx.lineTo(mousePos.x, mousePos.y);
    
  }
  if(line==true && (linef===1 || erass===0) && flag===1){
    //ctx.lineTo(mousePos.x, mousePos.y);
    //ctx.lineJoin = "round";
    var r=document.getElementById('red').value;
    var g=document.getElementById('green').value;
    var b=document.getElementById('blue').value;
    if(shadoww===1 && erass===0){
    ctx.shadowBlur = 10; 
    ctx.shadowColor = 'rgb(0, 0, 0)';
    } 
    ctx.lineTo(mousePos.x, mousePos.y);
    if(shadoww===0 && erass===0){
      ctx.shadowBlur=0;
    }
    if(gg===1 && erass===0){
    var grd=ctx.createLinearGradient(startX,startY,mousePos.x,mousePos.y);
      
    grd.addColorStop(0,"rgb("+r+","+g+","+b+")");
    if(r===g && g===b && b===r){
      grd.addColorStop(1,"white");
    }
    else {
      grd.addColorStop(1,"rgb(50,50,50)");
    }

    // Fill with gradient
    ctx.strokeStyle=grd;
    }

    if(gg===0 && erass===0){
      ctx.strokeStyle= "rgb("+r+","+g+","+b+")";
    }
  }


  if((circle==true && erass===0) || (erass===0 && cirf===1 && textt==false)&& flag===1){
  ctx.arc(startX,startY,(Math.sqrt((startX-mousePos.x)*(startX-mousePos.x)+(startY-mousePos.y)*(startY-mousePos.y))),0,2*Math.PI);
  var r=document.getElementById('red').value;
  var g=document.getElementById('green').value;
  var b=document.getElementById('blue').value;
  ctx.fillStyle = "rgb("+r+","+g+","+b+")";
  ctx.shadowBlur=0;
  ctx.fill();
  }

  if((rectangle==true && erass===0)|| (erass===0 && recf===1 && textt==false)&& flag===1){
    var r=document.getElementById('red').value;
    var g=document.getElementById('green').value;
    var b=document.getElementById('blue').value;
    ctx.shadowBlur=0;
    ctx.fillStyle = "rgb("+r+","+g+","+b+")";
    ctx.rect(startX,startY,Math.abs(mousePos.x-startX),Math.abs(mousePos.y-startY));
    ctx.fill();
  }

  if((triangle==true && erass===0)|| (erass===0 && trf===1 && textt==false)&& flag===1){
    var r=document.getElementById('red').value;
    var g=document.getElementById('green').value;
    var b=document.getElementById('blue').value;
    ctx.fillStyle = "rgb("+r+","+g+","+b+")";
    ctx.shadowBlur=0;
    ctx.moveTo(startX,startY);
    ctx.lineTo(mousePos.x,mousePos.y);
    ctx.lineTo(mousePos.x-2*(mousePos.x-startX),mousePos.y);
    ctx.lineTo(startX,startY);
    ctx.lineTo(mousePos.x,mousePos.y);
    ctx.fill();
  }
  
  ctx.stroke();
  //畫!
  
};

function erase(){
  erass=1;
  triangle=false;
  rectangle=false;
  circle=false;
  line=true;
  textt=false;
  imagg=false;
  flag=1;
  gg=0;
};

function brush(){
  erass=0;
  textt=false;
  imagg=false;
  line=true;
  flag=1;
  shadoww=0;
  gg=0;
  var r=document.getElementById('red').value;
  var g=document.getElementById('green').value;
  var b=document.getElementById('blue').value;

  ctx.strokeStyle = "rgb("+r+","+g+","+b+")";
  ctx.lineWidth=current;
};

function noshadow(){
  shadoww=0;
}

canvas.addEventListener('mousedown', function(evt) {
  var mousePos = getMousePos(canvas, evt);
  startX=mousePos.x;
  startY=mousePos.y;
  

  if(textt==true){
    var value = document.getElementById("text_value").value;
    var size = document.getElementById("text_size").value;
    
    var newSize = size + 'px';
    ctx.font = newSize + ' ' + "Georgia";
    //ctx.fillText(value,(startX+mousePos.x)/2,(startY+mousePos.y)/2);
    if(sf===0){
      ctx.fillText(value,mousePos.x,mousePos.y);
    }
    if(sf===1){
      ctx.strokeText(value,mousePos.x,mousePos.y);
    }
  }


  //從按下去就會執行第一次的座標取得
  evt.preventDefault();
  ctx.beginPath();
  //建立path物件
  ctx.moveTo(mousePos.x, mousePos.y);  
  //每次的點用moveTo去區別開，如果用lineTo會連在一起  
  canvas.addEventListener('mousemove', mouseMove, false);
  //mousemove的偵聽也在按下去的同時開啟
});
canvas.addEventListener('mouseup', function() {
  canvas.removeEventListener('mousemove', mouseMove, false);
  
  cPush();
}, false);
//如果滑鼠放開，將會停止mouseup的偵聽

// bind event handler to clear button
document.getElementById('clear').addEventListener('click', function() {
ctx.clearRect(0, 0, canvas.width, canvas.height);
}, false);



document.getElementById('small').addEventListener('click', function() {
  ctx.lineWidth = 1;
  current=1;
  }, false);
document.getElementById('mediam').addEventListener('click', function() {
  ctx.lineWidth = 4;
  current=4;
  }, false);
document.getElementById('large').addEventListener('click', function() {
  ctx.lineWidth = 10;
  current=10;
  }, false);


var color = document.getElementsByClassName("color");

var myColor = function() {
  var r=document.getElementById('red').value;
  var g=document.getElementById('green').value;
  var b=document.getElementById('blue').value;

  ctx.strokeStyle = "rgb("+r+","+g+","+b+")";
};


for (var i = 0; i < color.length; i++) {
  color[i].addEventListener('click', myColor, false);
}


document.getElementById('save').addEventListener('click', function() {
  var url = canvas.toDataURL();
  //利用toDataURL() 把canvas轉成data:image
  this.href = url;
  }, false);


function cPush() {
  cStep++;
  if (cStep < cPushArray.length) { cPushArray.length = cStep; }
  cPushArray.push(canvas.toDataURL());
  
}

function cRedo() {
  if (cStep < cPushArray.length-1) {
      cStep++;
      var img = new Image();
      img.src = cPushArray[cStep]
      img.onload = function(){
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img,0,0);
      }   
  }
}

function cUndo() {
  if (cStep > 0) {
      cStep--;
      var img = new Image();
      img.src = cPushArray[cStep]
      img.onload = function(){
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(img,0,0);
      }   
  }
  else if(cStep===0)
  {
    cStep--;
    ctx.clearRect(0, 0, canvas.width, canvas.height);
  }
}










