# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

功能描述:
預設是line brush
如果點erase會change to erase
若想再回去brush功能
必須要再點一次brush button
text以及image也是如果想切回brush or erase 
必須要再點brush or erase button

1. brush, brush size
2. erase(大小固定)
3. color 滑動rgb調整顏色
4. text, text size, 按下submit之後在canvas上任何一處點下去就有(可以一直貼text)
5. cursor icon: 按erase 滑鼠滑到canvas上會出現erase icon，按text 滑鼠滑到canvas上會出現text icon, 預設則為brush icon
6. refresh botton=clear button
7. brush shape: circle, triangle, rectangle
8. undo, redo button
9. image tool: 按下選擇檔案，可以從電腦裡選擇要上傳的圖片，upload完之後在canvas上任何一處點下去就有(可以一直貼text)
10. download=按save canvas

Other useful widgets:
1. brush shadow(only for line) 
2. text stroke vs fill
3. brush gradient(only for line)




